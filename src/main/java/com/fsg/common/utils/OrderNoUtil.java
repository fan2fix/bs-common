package com.fsg.common.utils;

import java.util.Date;
import java.util.Random;

public class OrderNoUtil {
	/** 
     * 获取16进制随机数 
     * @param len 长度
     * @return 
     * @throws CoderException 
     */  
    public static String randomHexString(int len)  {  
    	
        try {  
            StringBuffer result = new StringBuffer();  
            for(int i=0;i<len;i++) {  
                result.append(Integer.toHexString(new Random().nextInt(16)));  
            }  
            return result.toString().toUpperCase();  
        } catch (Exception e) {  
            return null;
        }  
          
    }
    
    
    /**
	 * 生成	订单编号
	 * 8位创建日期+6位	16进制随机生成数
	 * @param date
	 * @return
	 */
	public static String createOrderNo(String oderType ,Date date){
		String dateStr = DateUtil.defaultFormatSecondDate(date);
		return oderType+dateStr + randomHexString(6);
	} 
	
	
		public static void main(String[] args) {
			
			System.err.println(createOrderNo("GD",new Date()));
			
		}
	
	
}
