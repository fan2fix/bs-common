package com.fsg.common.request.exam;

import java.util.List;

import com.fsg.common.request.ReqPageVo;


public class ReqSubmitExam extends ReqPageVo {
	
	private Long examId;

    private List<String> answer;

	public List<String> getAnswer() {
		return answer;
	}

	public void setAnswer(List<String> answer) {
		this.answer = answer;
	}

	public Long getExamId() {
		return examId;
	}

	public void setExamId(Long examId) {
		this.examId = examId;
	}

	
    
    
}
