package com.fsg.common.request.exam;

import com.fsg.common.request.ReqPageVo;

import lombok.Data;

@Data
public class ReqExam extends ReqPageVo {

    private Long id;

   
    private String no;

   
    private Integer round;

  
    private String subject;

   
    private String userNo;

  
    private Integer score;
    
}
