package com.fsg.common.request.order;

import javax.validation.constraints.NotNull;

import com.fsg.common.request.ReqPageVo;

public class ReqOrderPageVo extends ReqPageVo {
	
	@NotNull(message = "{user.name.notBlank}")
    private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

  
    
    

}
