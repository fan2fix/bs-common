package com.fsg.common.request.order;

public class ReqOrderAddVo {

	 	private Long userId;

	    private Long videoId;

	    private String gorderNo;

	    private Long needPay;

	    private Long actualPay;

	    private Long couponId;

		public Long getUserId() {
			return userId;
		}

		public void setUserId(Long userId) {
			this.userId = userId;
		}

		public Long getVideoId() {
			return videoId;
		}

		public void setVideoId(Long videoId) {
			this.videoId = videoId;
		}

		public String getGorderNo() {
			return gorderNo;
		}

		public void setGorderNo(String gorderNo) {
			this.gorderNo = gorderNo;
		}

		public Long getNeedPay() {
			return needPay;
		}

		public void setNeedPay(Long needPay) {
			this.needPay = needPay;
		}

		public Long getActualPay() {
			return actualPay;
		}

		public void setActualPay(Long actualPay) {
			this.actualPay = actualPay;
		}

		public Long getCouponId() {
			return couponId;
		}

		public void setCouponId(Long couponId) {
			this.couponId = couponId;
		}

	
}
