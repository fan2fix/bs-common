package com.fsg.common.request.user;


import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

@Data
public class ReqUserRegisterVo {
	
		//@NotBlank(message = "{user.name.notBlank}")
		//@Pattern(regexp="/^[a-zA-Z0-9_-]{4,16}$/",message="{user.name.regexpErr}")
	 	private String name;
		
		//@NotBlank(message = "{user.password.notBlank}")
		//@Length(min=0,max=20,message = "{user.password.length}")
	    private String password;
		
		//@NotBlank(message = "{user.email.notBlank}")
		//@Email(message = "{user.email.regexpErr}")
	    private String email;
		
		//@NotBlank(message = "{user.wecharNo.notBlank}")
		//@Length(min=0,max=20,message = "{user.wechatNo.length}")
	    private String wechatNo;
		
		private String userName;

	    private String headPortrait;
		
		

	    
	    

	
}
