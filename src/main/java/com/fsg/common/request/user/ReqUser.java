package com.fsg.common.request.user;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class ReqUser {
	
    private String id;

    private String name;
    
    private String password;
    
    private String email;

    private String wechatNo;

    private String creator;

    private String userName;
    
    private String headPortrait;
    
    private String address;
    
    private String url;
    

}
