package com.fsg.common.request.video;

import com.fsg.common.request.ReqPageVo;

public class ReqVideoPageVo extends ReqPageVo {
	
    private String name;

    private String videoType;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVideoType() {
		return videoType;
	}

	public void setVideoType(String videoType) {
		this.videoType = videoType;
	}
    
    

}
