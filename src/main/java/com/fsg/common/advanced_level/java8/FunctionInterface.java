package com.fsg.common.advanced_level.java8;

import java.util.function.Consumer;

public class FunctionInterface {

	public static void main(String[] args) {
		Consumer c = new Consumer() {
		    @Override
		    public void accept(Object o) {
		        System.out.println(o);
		    }
		};
		String a="hello guo";
		//c.accept(a);
		c.accept(a);
		c.andThen(c);
	}
}
