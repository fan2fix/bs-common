package com.fsg.common.advanced_level.java8;

@FunctionalInterface
public interface ICustomerDemo<T> {

	void apply(T t);
}
