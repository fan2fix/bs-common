package com.fsg.common.advanced_level.thread;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 简单 execute 方式提交，不关心返回值的，直接往线程池里面扔任务
 * @author lfz
 *
 */
public class ThreadPoolExecutorDemo {
	
	public static void main(String[] args) throws Exception {
		//(2, 5, 60, TimeUnit.SECONDS, new LinkedBlockingQueue<>(10))
		// 2 :核心线程池大小
		// 5:最大线程池大小
		// 60:线程最大空闲时间
		// TimeUnit.SECONDS  :时间单位
		//new LinkedBlockingQueue<>(10) :线程等待队列
        ThreadPoolExecutor executor = new ThreadPoolExecutor(2, 5, 60l, TimeUnit.SECONDS, new LinkedBlockingQueue<>(10));
        //execute(Runnable command)方法。没有返回值
        executor.execute(() -> {
            System.out.println("关注why技术");
        });
        Thread.currentThread().join();
    }
}
