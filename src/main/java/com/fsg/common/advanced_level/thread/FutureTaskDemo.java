package com.fsg.common.advanced_level.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;


public class FutureTaskDemo {
	
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		System.err.println("任务1开始");
		FutureTask futureTask=new FutureTask(new Callable() {
	        @Override
	        public String call() throws Exception {
	            Thread.sleep(3000);
	            System.out.println("calld方法执行了");
	            return "call方法返回值";
	        }
	    });
	    futureTask.run();
	   
	    FutureTask futureTask1=new FutureTask(new Callable() {
	        @Override
	        public String call() throws Exception {
	            //Thread.sleep(3000);
	            System.out.println("calld方法执行了1");
	            return "call方法返回值1";
	        }
	    });
	    futureTask1.run();
	    System.out.println("获取返回值: " + futureTask.get());
	    System.out.println("获取返回值1: " + futureTask1.get());
	}
	
	
}

