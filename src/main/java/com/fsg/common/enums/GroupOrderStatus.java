package com.fsg.common.enums;


/**
 * 合购状态  
 * @author user
 *
 */
public enum GroupOrderStatus {
	CREATED("created","已创建"), 
	START("start","开始"),
	MAJORITY("majority","热销"),
	FULLED("fulled","满员"),
	PROCESSING("processing","处理中"),
	COMPLETED("completed","已完成");
	private String code;
	private String value;
	GroupOrderStatus(String code,String value) {
		this.value = value;
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public String getCode() {
		return code;
	}
	public String getValueByCode(String code) {
		for (GroupOrderStatus groupOrderStatus :GroupOrderStatus.values() ) {
			if(groupOrderStatus.value.equals(code)){
				return groupOrderStatus.value;
			}
		}
		return SysEnum.UNKOWN;
	}
}
