package com.fsg.common.response.user;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class RespUser {
	
    private String id;

    private String name;
    
    private String password;
    
    private String email;

    private String wechatNo;

    private String creator;

    private String userName;
    
    private String headPortrait;

    private LocalDateTime createdTime;

    private LocalDateTime modifiedTime;

}