package com.fsg.common.response.user;

import java.util.Date;

import lombok.Data;

@Data
public class ResUser {
	private Long id;

    private String name;
    
    private String userName;

    private String password;

    private String email;

    private String wechatNo;

    private String creator;

    private Date createdTime;

    private String modifier;

    private Date modifiedTime;
    
    private String role;
    
    private String headPortrait;

    
    
}
