package com.fsg.common.response.exam;

import java.io.Serializable;

public class ResGetExam  implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 404597962136357537L;


	private Long id;

   
    private String no;

   
    private Integer round;

  
    private String subject;

   
    private String userNo;

  
    private Integer score;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNo() {
		return no;
	}


	public void setNo(String no) {
		this.no = no;
	}


	public Integer getRound() {
		return round;
	}


	public void setRound(Integer round) {
		this.round = round;
	}


	public String getSubject() {
		return subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getUserNo() {
		return userNo;
	}


	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}


	public Integer getScore() {
		return score;
	}


	public void setScore(Integer score) {
		this.score = score;
	}
    
    
}
