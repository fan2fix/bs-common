package com.fsg.common.exception;

import java.text.MessageFormat;

import com.fsg.common.enums.ErrorEnum;



public class ParamException extends ProjException{
	
private static final long serialVersionUID = 7946023196149777499L;
	
	protected ErrorEnum errorCode;
	
	protected String errorMsg;
	
	protected Object[] arguments;


	public ErrorEnum getErrorCode() {
		return errorCode;
	}
	


	public ParamException(ErrorEnum ErrorEnum, String... arguments) {
		super();
		this.errorCode = ErrorEnum;
		this.realCode = ErrorEnum.getErrorCode();
		this.arguments =arguments;
	}
	
	
	public ParamException(ErrorEnum errorCode, Throwable cause) {
		super(cause);
		this.errorCode = errorCode;
		this.realCode = errorCode.getErrorCode();
	}
	
	public ParamException(String realCode , String errorMsg ,Object... arguments) {
		super();
		//this.errorCode = ErrorEnum;
		this.realCode =realCode;
		this.errorMsg =errorMsg;
		this.arguments =arguments;
	}
	
	@Override
	public String getMessage() {
		
		String notMessage = "not error, not message";
		String defaultMessage = "";
		
		if(errorMsg !=null){
			defaultMessage = errorMsg;
		}else{
			if (errorCode != null ) {
				defaultMessage = errorCode.getDefaultMessage();
			}
		}		
		if (defaultMessage==null||defaultMessage.trim() =="") {
			return notMessage;
		}
		return MessageFormat.format(defaultMessage, this.arguments);
	}
	
/*	public String getRealCode() {
		return realCode;
	}*/

	
	

}
