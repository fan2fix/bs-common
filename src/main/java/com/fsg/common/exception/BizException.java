package com.fsg.common.exception;

import java.text.MessageFormat;

import com.fsg.common.enums.ErrorEnum;



public class BizException extends ProjException{
	
private static final long serialVersionUID = 7946023196149777499L;
	
	protected ErrorEnum errorCode;
	
	protected String errorMsg;
	
	protected String[] arguments;
	
	protected String url;


	public ErrorEnum getErrorCode() {
		return errorCode;
	}
	
	   public BizException(String message) {
	        super(message);
	    }

	public BizException(ErrorEnum ErrorEnum, String... arguments) {
		super();
		this.errorCode = ErrorEnum;
		this.realCode = ErrorEnum.getErrorCode();
		this.arguments =arguments;
	}
	public BizException(ErrorEnum errorCode, Throwable cause) {
		super(cause);
		this.errorCode = errorCode;
		this.realCode = errorCode.getErrorCode();
	}
	@Override
	public String getMessage() {
		
		String notMessage = "not error, not message";
		String defaultMessage = "";
		
		if(errorMsg !=null){
			defaultMessage = errorMsg;
		}else{
			if (errorCode != null ) {
				defaultMessage = errorCode.getDefaultMessage();
			}
		}		
		if (defaultMessage==null||defaultMessage.trim() =="") {
			return notMessage;
		}
		return MessageFormat.format(defaultMessage, this.arguments);
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
/*	public String getRealCode() {
		return realCode;
	}*/

	
	

}
