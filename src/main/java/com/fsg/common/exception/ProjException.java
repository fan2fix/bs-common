package com.fsg.common.exception;

import com.fsg.common.enums.ErrorEnum;

/**
 * Created by gubaoer on 17/7/2.
 */


public class ProjException extends Exception {
	
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 7987257497528345630L;
	
	protected String realCode;    
	protected ErrorEnum errorCode;

    public ProjException(String message) {
        super(message);
    }

	public ProjException() {
		super();
	}
	
	public ProjException(ErrorEnum errorCode, Throwable cause) {
		super(cause);
		this.errorCode = errorCode;
		this.realCode = errorCode.getErrorCode();
	}

	public String getRealCode() {
		return realCode;
	}

	public void setRealCode(String realCode) {
		this.realCode = realCode;
	}

	public ProjException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ProjException(String message, Throwable cause) {
		super(message, cause);
	}

	public ProjException(Throwable cause) {
		super(cause);
	}


    
	

}
